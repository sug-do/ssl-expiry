### Features:
This code will deploy a serverless function to AWS lambda that will check the SSL expiry date of the fearless.tech domain. The function is written in Python. The resources are created in AWS using Terraform. The CI/CD leverages Gitlab CI runners.

### Assumptions:
- AWS account with appropriate IAM roles and permissions already configured to allow creation of resources and roles
- AWS credentials already configured
- Git access configured for updating code and/or pipeline in Gitlab
- AWS credentials configured in Gitlab for CI/CD
- Terraform 1.0+ installed
- Python 3.7+ installed
- Gitlab remote state configured for terraform https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html
    - Terraform is currently configured to use "ssl-expiry-state" state key

### Usage:
#### Use local filesystem for state file (local only):
1. Clone repository
2. Navigate to terraform directory
3. Remove backend.tf file
4. Run `terraform init`
5. Run `terraform apply`
6. Type `yes` and press Enter
7. Run `curl "$(terraform output -raw base_url)/fearless"` to view fearless.tech SSL expiration date

#### Use remote state file (local + Integrated pipeline deployments from Gitlab):
1. Configure remote state https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html
2. Push code to your Gitlab repository
- NOTE:
    - Any of the commands that can be run in the local steps can now be used and will sync changes to remote state.
    - In addition, any changes pushed to the repository will automatically run the Gitlab pipeline and update in AWS.

### Improvements:
- Variablize input to check any given domain name SSL expiration
- Implement remote storage for zip package
- Create additional environments/branches for dev, test, staging, etc
- Separate stages in CI/CD pipeline for validating and testing prior to allowing merge to main branch
- Modularize Terraform code
- Version control/locking
- Document more granular steps around permissions and IAM role setup to use least privilege