import socket
import ssl
import json

def ssl_expiry(domain):
    # create new context with secure default settings
    context = ssl.create_default_context()
    with socket.socket(socket.AF_INET) as sock:
        # use recommend method of wrap_socket of SSLContext to wrap sockets as SSLSocket objects
        with context.wrap_socket(sock, server_hostname=domain) as ssock:
            # set a reasonable timeout for the connection
            ssock.settimeout(1.0)
            # pass in domain and port
            ssock.connect((domain, 443))
            # retrieve certificate and store in variable
            ssl_info = ssock.getpeercert()

    # return the expiry date from the certificate info stored in variable
    return ssl_info['notAfter']

def lambda_handler(event, context):
    # store result of ssl expiry call
    result = ssl_expiry("fearless.tech")
    # return result to api gateway in json format
    return {
        "statusCode": 200,
        "body": json.dumps({"Fearless SSL expiration": result}),
        "headers": {
            "Content-Type": "application/json"
        }
    }