provider "aws" {
  region = "us-east-1"
}

data "archive_file" "lambda_ssl_expiry" {
  type = "zip"

  source_file = "${path.module}/../python/main.py"
  output_path = "${path.module}/ssl_expiry_python.zip"
}

resource "aws_lambda_function" "ssl_expiry" {
  function_name = "SSLExpiry"

  runtime = "python3.7"
  handler = "main.lambda_handler"

  filename = data.archive_file.lambda_ssl_expiry.output_path

  source_code_hash = data.archive_file.lambda_ssl_expiry.output_base64sha256

  role = aws_iam_role.lambda_exec.arn
}


resource "aws_cloudwatch_log_group" "ssl_expiry" {
  name = "/aws/lambda/${aws_lambda_function.ssl_expiry.function_name}"

  retention_in_days = 30
}

resource "aws_iam_role" "lambda_exec" {
  name = "serverless_lambda"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda_exec.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_apigatewayv2_api" "lambda" {
  name          = "ssl_expiry_lambda_gw"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_stage" "lambda" {
  api_id = aws_apigatewayv2_api.lambda.id

  name        = "ssl_expiry_lambda_stage"
  auto_deploy = true
}

resource "aws_apigatewayv2_integration" "ssl_expiry" {
  api_id = aws_apigatewayv2_api.lambda.id

  integration_uri    = aws_lambda_function.ssl_expiry.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "ssl_expiry" {
  api_id = aws_apigatewayv2_api.lambda.id

  route_key = "GET /fearless"
  target    = "integrations/${aws_apigatewayv2_integration.ssl_expiry.id}"
}

resource "aws_cloudwatch_log_group" "api_gw" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.lambda.name}"

  retention_in_days = 30
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ssl_expiry.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.lambda.execution_arn}/*/*"
}
